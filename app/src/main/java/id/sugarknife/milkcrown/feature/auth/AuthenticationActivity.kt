package id.sugarknife.milkcrown.feature.auth

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.androidadvance.topsnackbar.TSnackbar
import id.sugarknife.milkcrown.R
import id.sugarknife.milkcrown.base.BaseActivity
import id.sugarknife.milkcrown.feature.auth.checksession.CheckSessionFragment
import id.sugarknife.milkcrown.feature.auth.login.LoginFragment
import id.sugarknife.milkcrown.feature.dashboard.DashboardActivity
import id.sugarknife.milkcrown.util.error
import id.sugarknife.milkcrown.util.withPaddingTop
import kotlinx.android.synthetic.main.activity_authentication.*

class AuthenticationActivity : BaseActivity(), AuthenticationFeature.View {

    private val dimDialog: Dialog by lazy {
        val dialog = Dialog(this)
        dialog.setCancelable(false)
        dialog
    }

    private val checkSessionFragment: CheckSessionFragment by lazy {
        CheckSessionFragment()
    }

    private val loginFragment: LoginFragment by lazy {
        LoginFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authentication)

        supportFragmentManager.beginTransaction()
                .replace(R.id.fragmentLayout, checkSessionFragment)
                .commit()
    }

    override fun showDim() {
        dimDialog.show()
    }

    override fun dismissDim() {
        dimDialog.dismiss()
    }

    override fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    override fun showError(errorMessage: String) {
        TSnackbar.make(findViewById(android.R.id.content), errorMessage, TSnackbar.LENGTH_SHORT)
                .error()
                .withPaddingTop()
                .show()
    }

    override fun showLoginForm() {
        supportFragmentManager.beginTransaction()
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .replace(R.id.fragmentLayout, loginFragment)
                .commit()
    }

    override fun routeToDashboard() {
        val intent = Intent(this, DashboardActivity::class.java)
        startActivity(intent)
        finish()
    }
}
