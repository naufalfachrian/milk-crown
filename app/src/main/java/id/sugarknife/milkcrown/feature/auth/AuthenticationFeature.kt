package id.sugarknife.milkcrown.feature.auth

class AuthenticationFeature {

    interface View {
        fun showDim()
        fun dismissDim()
        fun showProgressBar()
        fun hideProgressBar()
        fun showError(errorMessage: String)
        fun showLoginForm()
        fun routeToDashboard()
    }

    open class Fragment : android.support.v4.app.Fragment() {

        protected val authView: AuthenticationFeature.View?
            get() {
                return activity as? AuthenticationFeature.View
            }

    }

}