package id.sugarknife.milkcrown.feature.dashboard.timeline

import android.content.Context
import id.sugarknife.milkcrown.model.milkcrown.MediaViewModel

class TimelineFeature {

    interface View {
        fun context(): Context
        fun bindAdapter(adapter: TimelineAdapter)
        fun showLineOnFooterView()
        fun hideNextPageButtonLayout()
        fun showNextPageButtonLayout()
        fun hideProgressOnFooterView()
        fun showProgressOnFooterView()
        fun showMediaInfo(media: MediaViewModel)
    }

    interface Presenter {
        fun requestAdapter()
        fun fetchTimeline()
        fun viewDidScrollDown()
        fun nextPageButtonClicked()
    }

    interface AdapterDelegate {
        fun mediaCoverClicked(media: MediaViewModel)
    }

}