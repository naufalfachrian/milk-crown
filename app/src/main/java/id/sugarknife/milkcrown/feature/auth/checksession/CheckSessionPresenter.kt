package id.sugarknife.milkcrown.feature.auth.checksession

import android.util.Log
import id.sugarknife.milkcrown.controller.checkAndRefreshSessionIfNeeded
import id.sugarknife.milkcrown.controller.loadSession
import id.sugarknife.milkcrown.controller.saveSession
import id.sugarknife.milkcrown.util.MilkCrown
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class CheckSessionPresenter : CheckSessionFeature.Presenter {

    override fun checkAndRefreshSessionIfNeeded(view: CheckSessionFeature.View) {
        view.whenSessionIsRefreshing()
        MilkCrown.Kitsu.Auth.loadSession(view.context())
                .subscribeOn(Schedulers.io())
                .flatMap {
                    MilkCrown.Kitsu.Auth.checkAndRefreshSessionIfNeeded(it)
                }
                .flatMap {
                    MilkCrown.Kitsu.Auth.saveSession(view.context(), it)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.whenSessionHasRefreshed()
                    view.whenSessionAvailable()
                }, {
                    view.whenSessionHasRefreshed()
                    view.whenSessionNotAvailable()
                })
    }
}