package id.sugarknife.milkcrown.feature.dashboard.timeline

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.view.ViewCompat
import android.support.v4.widget.NestedScrollView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.sugarknife.milkcrown.R
import id.sugarknife.milkcrown.feature.dashboard.DashboardFeature
import id.sugarknife.milkcrown.feature.media.MediaActivity
import id.sugarknife.milkcrown.model.milkcrown.MediaViewModel
import id.sugarknife.milkcrown.util.CustomLinearLayoutManager
import kotlinx.android.synthetic.main.fragment_timeline.*

class TimelineFragment : DashboardFeature.Fragment(), TimelineFeature.View {

    private val presenter by lazy {
        TimelinePresenter(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_timeline, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter.requestAdapter()

        nestedScrollView.setOnScrollChangeListener {
            v: NestedScrollView, _: Int, scrollY: Int, _: Int, oldScrollY: Int ->
            val childView = v.getChildAt(0) ?: return@setOnScrollChangeListener
            val diff = childView.bottom - (v.height + scrollY)
            if (diff < 16) {
                presenter.viewDidScrollDown()
            }
            if (scrollY > oldScrollY) {
                dashboardView?.hideCreatePostButton()
            }
            if (scrollY < oldScrollY) {
                dashboardView?.showCreatePostButton()
            }
        }

        nextPageButton.setOnClickListener {
            presenter.nextPageButtonClicked()
        }

        recyclerView.layoutManager = CustomLinearLayoutManager(context)
        ViewCompat.setNestedScrollingEnabled(recyclerView, false)
        
        recyclerView.setItemViewCacheSize(20)
        recyclerView.isDrawingCacheEnabled = true
        recyclerView.drawingCacheQuality = View.DRAWING_CACHE_QUALITY_HIGH

        presenter.fetchTimeline()
    }

    override fun context(): Context {
        return context!!
    }

    override fun bindAdapter(adapter: TimelineAdapter) {
        recyclerView.adapter = adapter
    }

    override fun showLineOnFooterView() {
        progressTimeline.visibility = View.VISIBLE
    }

    override fun hideNextPageButtonLayout() {
        nextPageButtonLayout.visibility = View.GONE
    }

    override fun showNextPageButtonLayout() {
        nextPageButtonLayout.visibility = View.VISIBLE
    }

    override fun hideProgressOnFooterView() {
        progressBarLayout.visibility = View.GONE
    }

    override fun showProgressOnFooterView() {
        progressBarLayout.visibility = View.VISIBLE
    }

    override fun showMediaInfo(media: MediaViewModel) {
        val intent = Intent(context, MediaActivity::class.java)
        intent.putExtra("mediaType", media.type)
        intent.putExtra("mediaId", media.id)
        intent.putExtra("mediaTitle", media.title)
        intent.putExtra("mediaPoster", media.posterUrl)
        startActivity(intent)
    }

}
