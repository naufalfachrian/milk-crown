package id.sugarknife.milkcrown.feature.dashboard

class DashboardFeature {

    interface View {
        fun showCreatePostButton()
        fun hideCreatePostButton()
    }

    open class Fragment : android.support.v4.app.Fragment() {

        protected val dashboardView: DashboardFeature.View?
            get() {
                return activity as? DashboardFeature.View
            }

    }

}