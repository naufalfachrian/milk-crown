package id.sugarknife.milkcrown.feature.auth.login

import android.content.Context

class LoginFeature {

    interface View {
        fun inputUsername(): String
        fun inputPassword(): String
        fun whenLoginInProgress()
        fun whenLoginFailed(reason: String)
        fun whenLoginSuccess()
        fun context(): Context
    }

    interface Presenter {
        fun login(view: View)
    }

}