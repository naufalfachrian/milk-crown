package id.sugarknife.milkcrown.feature.dashboard

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import id.sugarknife.milkcrown.R
import id.sugarknife.milkcrown.base.BaseActivity
import id.sugarknife.milkcrown.feature.createpost.CreatePostActivity
import id.sugarknife.milkcrown.feature.dashboard.timeline.TimelineFragment
import id.sugarknife.milkcrown.feature.search.SearchActivity
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : BaseActivity(), DashboardFeature.View {

    private val timelineFragment by lazy {
        TimelineFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        title = ""
        setSupportActionBar(customToolbar)

        supportFragmentManager.beginTransaction()
                .replace(R.id.fragmentLayout, timelineFragment)
                .commit()

        fab.setOnClickListener {
            startActivity(Intent(this, CreatePostActivity::class.java))
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.dashboard, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menuSearch -> startActivity(Intent(this, SearchActivity::class.java))
        }
        return true
    }

    override fun hideCreatePostButton() {
        fab.hide()
    }

    override fun showCreatePostButton() {
        fab.show()
    }
}
