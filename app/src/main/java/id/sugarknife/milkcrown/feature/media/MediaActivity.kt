package id.sugarknife.milkcrown.feature.media

import android.graphics.Bitmap
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.request.RequestOptions
import id.sugarknife.milkcrown.R
import id.sugarknife.milkcrown.base.BaseActivity
import id.sugarknife.milkcrown.feature.media.anime.AnimeDetailFragment
import kotlinx.android.synthetic.main.activity_media.*

class MediaActivity : BaseActivity(), MediaFeature.View {

    private val animeDetailFragment by lazy {
        AnimeDetailFragment()
    }

    private var mediaId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_media)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_back_24dp)
        title = ""

        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                .or(View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION)
                .or(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)
                .or(View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)

        mediaId = intent.extras.get("mediaId") as? String ?: ""
        val mediaType = intent.extras.get("mediaType") as? String
        val mediaCover = intent.extras.get("mediaPoster") as? String

        Glide.with(this).load(mediaCover)
                .apply(RequestOptions()
                        .centerInside()
                        .placeholder(R.drawable.placeholder_image)
                        .error(R.drawable.placeholder_image)
                        .encodeFormat(Bitmap.CompressFormat.JPEG)
                        .format(DecodeFormat.PREFER_RGB_565))
                .into(coverImageView)

        if (mediaType == "Anime") {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.mediaDetailFragment, animeDetailFragment)
                    .commit()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }

    override fun mediaId(): String {
        return mediaId
    }

    override fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    override fun setTitle(title: String) {
        titleLabel.text = title
    }

}
