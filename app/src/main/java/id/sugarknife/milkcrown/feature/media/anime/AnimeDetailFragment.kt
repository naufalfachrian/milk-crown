package id.sugarknife.milkcrown.feature.media.anime

import android.os.Build
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.sugarknife.milkcrown.R
import id.sugarknife.milkcrown.feature.media.MediaFeature
import id.sugarknife.milkcrown.feature.media.review.ReviewAdapter
import id.sugarknife.milkcrown.model.kitsu.AnimeModel
import id.sugarknife.milkcrown.model.kitsu.ReviewModel
import kotlinx.android.synthetic.main.layout_media_anime_legth.*
import kotlinx.android.synthetic.main.layout_media_community_approval.*
import kotlinx.android.synthetic.main.layout_media_highest_rank.*
import kotlinx.android.synthetic.main.layout_media_most_popular.*
import kotlinx.android.synthetic.main.layout_media_rating_guide.*
import kotlinx.android.synthetic.main.layout_media_review.*
import kotlinx.android.synthetic.main.layout_media_review_count.*
import kotlinx.android.synthetic.main.layout_media_synopsis.*
import kotlinx.android.synthetic.main.layout_media_timespan.*
import kotlinx.android.synthetic.main.layout_media_title_canonical.*
import kotlinx.android.synthetic.main.layout_media_title_english.*
import kotlinx.android.synthetic.main.layout_media_title_japanese.*
import kotlinx.android.synthetic.main.layout_media_title_romaji.*

class AnimeDetailFragment : MediaFeature.Fragment(), AnimeDetailFeature.View {

    private val presenter by lazy {
        AnimeDetailPresenter()
    }

    private val reviewAdapter by lazy {
        ReviewAdapter()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_anime_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val unwrappedMediaView = mediaView
        if (unwrappedMediaView != null) {
            presenter.fetchAnimeDetail(unwrappedMediaView.mediaId(), this)
        }
        reviewRecyclerView.adapter = reviewAdapter
        reviewRecyclerView.layoutManager = LinearLayoutManager(context)
    }

    override fun showProgressBarUnderCover() {
        mediaView?.showProgressBar()
    }

    override fun hideProgressBarUnderCover() {
        mediaView?.hideProgressBar()
    }

    override fun displayAnimeDetail(anime: AnimeModel) {
        mediaView?.setTitle(anime.canonicalTitle ?: "")
        val formattedTitle = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(anime.formattedTitle(), Html.FROM_HTML_MODE_LEGACY)
        } else {
            @Suppress("DEPRECATION")
            Html.fromHtml(anime.formattedTitle())
        }
        titleCanonicalLabel.text = formattedTitle
        titleEnglishLabel.text = anime.titles?.englishTitle
        titleJapaneseLabel.text = anime.titles?.japaneseTitle
        titleRomajiLabel.text = anime.titles?.romajiTitle
        synopsisLabel.text = anime.synopsis

        if (!anime.averageRating.isNullOrEmpty()) {
            communityApprovalLabel.text = getString(R.string.community_approval_2, anime.averageRating)
        }

        if (anime.popularityRank != null || anime.popularityRank != 0) {
            mostPopularLabel.text = getString(R.string.most_popular_2, anime.popularityRank, "Anime")
        }

        if (anime.ratingRank != null || anime.ratingRank != 0) {
            highestRankLabel.text = getString(R.string.most_highest_rank_2, anime.ratingRank, "Anime")
        }

        val episodeCount = anime.episodeCount ?: 0
        var episodeDetail = if (episodeCount != 0) {
            resources.getQuantityString(R.plurals.anime_episode_detail, episodeCount, episodeCount, anime.subtype ?: "")
        } else { "" }

        val episodeLength = anime.episodeLength ?: 0
        val episodeLengthDetail = if (episodeLength != 0) {
            getString(R.string.anime_length_detail, episodeLength)
        } else { "" }

        if (episodeLengthDetail.isNotEmpty()) {
            if (episodeDetail.isEmpty()) {
                episodeDetail = episodeLengthDetail
            } else {
                episodeDetail += " @ "
                episodeDetail += episodeLengthDetail
            }
        }
        animeLengthLabel.text = episodeDetail

        val startedDate = anime.formattedStartDate()
        var timespanDetail = if (startedDate.isNotEmpty()) {
            getString(R.string.anime_timespan_detail, startedDate)
        } else { "" }

        if (timespanDetail.isNotEmpty() && anime.formattedEndDate().isNotEmpty()) {
            timespanDetail += " till "
            timespanDetail += anime.formattedEndDate()
        }
        timespanLabel.text = timespanDetail

        if (anime.ageRatingGuide?.contains("Children") == true) {
            ratingGuideIcon.setBackgroundResource(R.drawable.ic_safe_for_child)
        } else {
            ratingGuideIcon.setBackgroundResource(R.drawable.ic_not_safe_for_child)
        }
        ratingGuideLabel.text = anime.ageRatingGuide

        presenter.fetchAnimeReviews(anime.id ?: "", this)
        Log.d("milk-crown", "Anime ID : ${anime.id}")
    }

    override fun displayAnimeReviewCount(reviewCount: Int) {
        reviewCountLabel.text = "There are $reviewCount reviews"
    }

    override fun appendAnimeReviews(reviews: List<ReviewModel>) {
        reviewAdapter.append(reviews)
    }

}
