package id.sugarknife.milkcrown.feature.media.review

import android.os.Build
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import id.sugarknife.milkcrown.R
import id.sugarknife.milkcrown.model.kitsu.ReviewModel
import id.sugarknife.milkcrown.util.formatBold
import id.sugarknife.milkcrown.util.printTimestamp
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ReviewAdapter : RecyclerView.Adapter<ReviewAdapter.ViewHolder>() {

    private var reviews = ArrayList<ReviewModel>()

    fun append(appendedReviews: List<ReviewModel>) {
        appendedReviews.forEach {
            Schedulers.io().createWorker().schedule {
                reviews.add(it)
                AndroidSchedulers.mainThread().createWorker().schedule {
                    notifyItemInserted(reviews.count() - 1)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return reviews.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.review_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val review = reviews[position]
        holder?.bind(review)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(review: ReviewModel) {

            Schedulers.io().createWorker().schedule {
                val ratingCaptionSpan = "${(review.rating * 5).toInt()}%".formatBold("#FF5722")
                val titleCaptionSpan = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    Html.fromHtml("${review.createdBy?.formattedName()} rated $ratingCaptionSpan",
                            Html.FROM_HTML_MODE_LEGACY)
                } else {
                    @Suppress("DEPRECATION")
                    Html.fromHtml("${review.createdBy?.formattedName()} rated $ratingCaptionSpan")
                }

                val timestamp = printTimestamp(review.createdAt, review.updatedAt)

                val reviewContent = review.content ?: ""
                val reviewSummary = if (reviewContent.length > 100) {
                    reviewContent.subSequence(0 .. 100)
                } else {
                    reviewContent
                }

                AndroidSchedulers.mainThread().createWorker().schedule {
                    val titleLabel = itemView.findViewById<TextView>(R.id.titleCaption)
                    titleLabel.text = titleCaptionSpan

                    val userAvatarView = itemView.findViewById<ImageView>(R.id.userAvatarView)
                    Glide.with(itemView)
                            .load(review.createdBy?.avatar?.small)
                            .apply(RequestOptions().circleCrop())
                            .into(userAvatarView)

                    val timestampCaption = itemView.findViewById<TextView>(R.id.timestampCaption)
                    timestampCaption.text = timestamp

                    val reviewOverviewLabel = itemView.findViewById<TextView>(R.id.reviewOverviewLabel)
                    reviewOverviewLabel.text = "$reviewSummary..."
                }

            }
        }

    }

}