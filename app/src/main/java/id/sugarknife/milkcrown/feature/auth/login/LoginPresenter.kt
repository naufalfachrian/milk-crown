package id.sugarknife.milkcrown.feature.auth.login

import id.sugarknife.milkcrown.controller.login
import id.sugarknife.milkcrown.controller.saveSession
import id.sugarknife.milkcrown.util.MilkCrown
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class LoginPresenter : LoginFeature.Presenter {

    override fun login(view: LoginFeature.View) {
        view.whenLoginInProgress()
        MilkCrown.Kitsu.Auth.login(view.inputUsername(), view.inputPassword())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap {
                    MilkCrown.Kitsu.Auth.saveSession(view.context(), it)
                }
                .subscribe({
                    view.whenLoginSuccess()
                }, {
                    view.whenLoginFailed(it.localizedMessage)
                })
    }

}
