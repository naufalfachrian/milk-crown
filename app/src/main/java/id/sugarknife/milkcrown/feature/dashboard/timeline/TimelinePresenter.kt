package id.sugarknife.milkcrown.feature.dashboard.timeline

import android.util.Log
import id.sugarknife.milkcrown.controller.fetchTimeline
import id.sugarknife.milkcrown.model.milkcrown.MediaViewModel
import id.sugarknife.milkcrown.model.milkcrown.TimelineViewModel
import id.sugarknife.milkcrown.util.MilkCrown
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class TimelinePresenter(val view: TimelineFeature.View) : TimelineFeature.Presenter, TimelineFeature.AdapterDelegate {

    private var isFetching = false

    private val pendingItems = ArrayList<TimelineViewModel>()

    private var lastCursor = ""

    private val adapter by lazy {
        TimelineAdapter()
    }

    override fun requestAdapter() {
        adapter.delegate = this
        view.bindAdapter(adapter)
    }

    override fun fetchTimeline() {
        if (isFetching || pendingItems.isNotEmpty()) return
        isFetching = true
        MilkCrown.Kitsu.Timeline.fetchTimeline(view.context(), lastCursor)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (lastCursor.isEmpty()) {
                        view.showLineOnFooterView()
                        adapter.append(it.timelineList)
                    } else {
                        pendingItems.addAll(it.timelineList)
                        adapterHasPendingUpdate(true)
                    }
                    isFetching = false
                    lastCursor = it.lastCursor
                    Log.i("milk-crown", "${it.timelineList.count()}")
                    Log.i("milk-crown", "lastCursor : ${it.lastCursor}")
                }, {
                    isFetching = false
                    Log.e("milk-crown", it.localizedMessage)
                })
    }

    override fun viewDidScrollDown() {
        fetchTimeline()
    }

    private fun adapterHasPendingUpdate(hasPendingUpdate: Boolean) {
        if (hasPendingUpdate) {
            view.showNextPageButtonLayout()
            view.hideProgressOnFooterView()
        } else {
            view.hideNextPageButtonLayout()
            view.showProgressOnFooterView()
        }
    }

    override fun mediaCoverClicked(media: MediaViewModel) {
        view.showMediaInfo(media)
    }

    override fun nextPageButtonClicked() {
        adapterHasPendingUpdate(false)
        adapter.append(pendingItems)
        pendingItems.clear()
    }
}