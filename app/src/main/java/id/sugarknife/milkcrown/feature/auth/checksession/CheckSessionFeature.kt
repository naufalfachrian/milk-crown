package id.sugarknife.milkcrown.feature.auth.checksession

import android.content.Context

class CheckSessionFeature {

    interface View {
        fun context(): Context
        fun whenSessionAvailable()
        fun whenSessionNotAvailable()
        fun whenSessionIsRefreshing()
        fun whenSessionHasRefreshed()
    }

    interface Presenter {
        fun checkAndRefreshSessionIfNeeded(view: View)
    }

}