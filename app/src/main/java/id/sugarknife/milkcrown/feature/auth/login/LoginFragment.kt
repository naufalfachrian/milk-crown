package id.sugarknife.milkcrown.feature.auth.login

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.sugarknife.milkcrown.R
import id.sugarknife.milkcrown.feature.auth.AuthenticationFeature
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : AuthenticationFeature.Fragment(), LoginFeature.View {

    private val presenter by lazy {
        LoginPresenter()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loginButton.setOnClickListener {
            presenter.login(this)
        }
    }

    override fun inputUsername(): String {
        return inputUsername.text.toString()
    }

    override fun inputPassword(): String {
        return inputPassword.text.toString()
    }

    override fun whenLoginInProgress() {
        authView?.showDim()
        authView?.showProgressBar()
    }

    override fun whenLoginFailed(reason: String) {
        authView?.dismissDim()
        authView?.hideProgressBar()
        authView?.showError(reason)
    }

    override fun whenLoginSuccess() {
        authView?.dismissDim()
        authView?.hideProgressBar()
        authView?.routeToDashboard()
    }

    override fun context(): Context {
        return context!!
    }

}
