package id.sugarknife.milkcrown.feature.media.anime

import id.sugarknife.milkcrown.model.kitsu.AnimeModel
import id.sugarknife.milkcrown.model.kitsu.ReviewModel

class AnimeDetailFeature {

    interface View {
        fun showProgressBarUnderCover()
        fun hideProgressBarUnderCover()
        fun displayAnimeDetail(anime: AnimeModel)
        fun displayAnimeReviewCount(reviewCount: Int)
        fun appendAnimeReviews(reviews: List<ReviewModel>)
    }

    interface Presenter {
        fun fetchAnimeDetail(animeId: String, view: View)
        fun fetchAnimeReviews(animeId: String, view: AnimeDetailFeature.View)
    }

}