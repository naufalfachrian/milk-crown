package id.sugarknife.milkcrown.feature.media

class MediaFeature {

    interface View {
        fun mediaId(): String
        fun showProgressBar()
        fun hideProgressBar()
        fun setTitle(title: String)
    }

    open class Fragment : android.support.v4.app.Fragment() {

        protected val mediaView: MediaFeature.View?
            get() {
                return activity as? MediaFeature.View
            }

    }

}