package id.sugarknife.milkcrown.feature.auth.checksession

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.sugarknife.milkcrown.R
import id.sugarknife.milkcrown.feature.auth.AuthenticationFeature

class CheckSessionFragment : AuthenticationFeature.Fragment(), CheckSessionFeature.View {

    private val presenter: CheckSessionPresenter by lazy {
        CheckSessionPresenter()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_check_session, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.checkAndRefreshSessionIfNeeded(this)
    }

    override fun context(): Context {
        return context!!
    }

    override fun whenSessionAvailable() {
        Handler().postDelayed({
            authView?.routeToDashboard()
        }, 1000)
    }

    override fun whenSessionNotAvailable() {
        Handler().postDelayed({
            authView?.showLoginForm()
        }, 1000)
    }

    override fun whenSessionIsRefreshing() {
        authView?.showProgressBar()
    }

    override fun whenSessionHasRefreshed() {
        authView?.hideProgressBar()
    }
}
