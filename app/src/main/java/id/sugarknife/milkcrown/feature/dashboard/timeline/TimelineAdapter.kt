package id.sugarknife.milkcrown.feature.dashboard.timeline

import android.graphics.Bitmap
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy
import com.bumptech.glide.request.RequestOptions
import id.sugarknife.milkcrown.R
import id.sugarknife.milkcrown.model.milkcrown.TimelineType
import id.sugarknife.milkcrown.model.milkcrown.TimelineViewModel
import id.sugarknife.milkcrown.util.DisplaySize
import id.sugarknife.milkcrown.util.RemoteBitmapSpanTarget
import io.square1.richtextlib.ui.RichContentView

class TimelineAdapter : RecyclerView.Adapter<TimelineAdapter.ViewHolder>() {

    private val items = ArrayList<TimelineViewModel>()

    var delegate: TimelineFeature.AdapterDelegate? = null

    private val imageQuality = 1

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val item = items[position]
        when (holder?.itemViewType) {
            TimelineType.LibraryEntry -> holder.bindLibraryEntry(item)
            TimelineType.MediaReaction -> holder.bindMediaReaction(item)
            TimelineType.Post -> holder.bindPost(item)
        }
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun getItemViewType(position: Int): Int {
        return items[position].type
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val layout: Int = when (viewType) {
            TimelineType.LibraryEntry -> R.layout.timeline_item_library_entry
            TimelineType.MediaReaction -> R.layout.timeline_item_media_reaction
            TimelineType.Post -> R.layout.timeline_item_post
            else -> R.layout.timeline_item_post
        }
        return ViewHolder(LayoutInflater.from(parent?.context).inflate(layout, parent, false))
    }

    override fun onViewRecycled(holder: ViewHolder?) {
        holder?.clear()
        super.onViewRecycled(holder)
    }

    fun append(list: List<TimelineViewModel>) {
        items.addAll(list)
        notifyItemRangeInserted(items.count() - list.count(), items.count())
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val userAvatarView = itemView.findViewById<ImageView?>(R.id.userAvatarView)
        private val titleCaptionView = itemView.findViewById<TextView?>(R.id.titleCaption)
        private val timestampCaptionView = itemView.findViewById<TextView?>(R.id.timestampCaption)

        private val coverImageView = itemView.findViewById<ImageView?>(R.id.coverImageView)
        private val averageScoreLabel = itemView.findViewById<TextView?>(R.id.averageScoreLabel)
        private val popularityRankLabel = itemView.findViewById<TextView?>(R.id.popularityRankLabel)
        private val scoreRankLabel = itemView.findViewById<TextView?>(R.id.scoreRankLabel)
        private val popularityLabel = itemView.findViewById<TextView?>(R.id.popularityLabel)
        private val scoreLabel = itemView.findViewById<TextView?>(R.id.scoreLabel)

        private val descriptionCaptionView = itemView.findViewById<RichContentView?>(R.id.descriptionCaption)

        private val activityLikesLabel = itemView.findViewById<TextView?>(R.id.activityLikesLabel)
        private val activityReactionLabel = itemView.findViewById<TextView?>(R.id.activityCommentsLabel)

        private val mediaLayout = itemView.findViewById<View?>(R.id.mediaLayout)

        private val screenSize = DisplaySize.getScreenSize(itemView.context)

        fun clear() {
            Log.d("milk-crown", "clearing view holder")
            if (userAvatarView != null) {
                Glide.with(userAvatarView).clear(userAvatarView)
            }
            if (coverImageView != null) {
                Glide.with(coverImageView).clear(coverImageView)
            }
            descriptionCaptionView?.setText("")
        }

        private fun bindBasicInfo(item: TimelineViewModel) {
            titleCaptionView?.text = item.title
            timestampCaptionView?.text = item.timestamp
            if (userAvatarView != null) {
                Glide.with(userAvatarView)
                        .load(item.userAvatarUrl)
                        .apply(RequestOptions().circleCrop()
                                .encodeQuality(imageQuality)
                                .encodeFormat(Bitmap.CompressFormat.JPEG)
                                .downsample(DownsampleStrategy.AT_MOST)
                                .format(DecodeFormat.PREFER_RGB_565))
                        .into(userAvatarView)
            }
        }

        private fun bindMediaInfo(item: TimelineViewModel) {
            averageScoreLabel?.text = if (item.media.score.isEmpty()) { "-" }
            else { "${item.media.score} %" }

            scoreRankLabel?.text = if (item.media.scoreRank.isEmpty()) { "-" }
            else { "Rank #${item.media.scoreRank}" }

            popularityRankLabel?.text = if (item.media.popularityRank.isEmpty()) { "-" }
            else { "Rank #${item.media.popularityRank}" }

            popularityLabel?.text = itemView.context.getString(R.string.most_popular, item.media.type)
            scoreLabel?.text = itemView.context.getString(R.string.most_highest_rank, item.media.type)

            if (coverImageView != null) {
                Glide.with(coverImageView)
                        .load(item.media.coverUrl)
                        .apply(RequestOptions()
                                .placeholder(R.drawable.placeholder_media)
                                .error(R.drawable.placeholder_media)
                                .encodeQuality(imageQuality)
                                .encodeFormat(Bitmap.CompressFormat.JPEG)
                                .centerCrop()
                                .downsample(DownsampleStrategy.AT_MOST)
                                .format(DecodeFormat.PREFER_RGB_565))
                        .into(coverImageView)
            }

            coverImageView?.setOnClickListener {
                delegate?.mediaCoverClicked(item.media)
            }
        }

        private fun bindDescription(item: TimelineViewModel) {
            if (descriptionCaptionView != null) {
                descriptionCaptionView.setFontFamily("fonts/delius-regular.ttf")
                descriptionCaptionView.setUrlBitmapDownloader { urlBitmapSpan, image ->
                    Glide.with(descriptionCaptionView)
                            .load(image)
                            .apply(RequestOptions()
                                    .placeholder(R.drawable.placeholder_image)
                                    .error(R.drawable.placeholder_image)
                                    .encodeQuality(imageQuality)
                                    .override(screenSize.x * 82 / 100, screenSize.y * 82 / 100)
                                    .encodeFormat(Bitmap.CompressFormat.JPEG)
                                    .dontTransform()
                                    .downsample(DownsampleStrategy.AT_MOST)
                                    .format(DecodeFormat.PREFER_RGB_565))
                            .into(RemoteBitmapSpanTarget(urlBitmapSpan, descriptionCaptionView.context))
                }
                descriptionCaptionView.setText(item.content)
            }
        }

//        private fun bindEmbeddedContent(item: TimelineContent) {
//            val embedImageView = itemView.findViewById<ImageView>(R.id.embedImage)
//            val embedDescriptionView = itemView.findViewById<TextView>(R.id.embedDescriptionLabel)
//
//            if (item.embedDescription() != null) {
//                embedDescriptionView.text = item.embedDescription()
//            }
//
//            if (item.embedUrl() != null) {
//                Picasso.with(itemView.context)
//                        .load(item.embedUrl())
//                        .into(embedImageView)
//            }
//        }

        private fun bindActivityLikes(item: TimelineViewModel) {
            activityLikesLabel?.text = itemView.context.resources
                    .getQuantityString(R.plurals.activity_likes, item.likesCount, item.likesCount)
        }

        private fun bindActivityReaction(item: TimelineViewModel) {
            activityReactionLabel?.text = itemView.context.resources
                    .getQuantityString(R.plurals.activity_reaction, item.reactionCount, item.reactionCount)
        }

        fun bindLibraryEntry(item: TimelineViewModel) {
            bindBasicInfo(item)
            bindMediaInfo(item)
        }

        fun bindMediaReaction(item: TimelineViewModel) {
            bindLibraryEntry(item)
            bindDescription(item)
            bindActivityLikes(item)
        }

        fun bindPost(item: TimelineViewModel) {
            bindBasicInfo(item)
            bindDescription(item)
//            val embedContentLayout = itemView.findViewById<View>(R.id.embedContentLayout)
//            embedContentLayout.visibility = item.embedVisibility()
//            if (embedContentLayout.visibility == View.VISIBLE) {
//                bindEmbeddedContent(item)
//            }
            bindActivityLikes(item)
            bindActivityReaction(item)

            if (item.media.title.isNotEmpty()) {
                mediaLayout?.visibility = View.VISIBLE
                bindMediaInfo(item)
            } else {
                mediaLayout?.visibility = View.GONE
            }
        }

    }

}