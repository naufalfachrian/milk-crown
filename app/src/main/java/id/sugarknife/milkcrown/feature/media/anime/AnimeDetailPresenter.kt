package id.sugarknife.milkcrown.feature.media.anime

import android.util.Log
import id.sugarknife.milkcrown.controller.find
import id.sugarknife.milkcrown.controller.findReview
import id.sugarknife.milkcrown.util.MilkCrown
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException

class AnimeDetailPresenter : AnimeDetailFeature.Presenter {

    override fun fetchAnimeDetail(animeId: String, view: AnimeDetailFeature.View) {
        view.showProgressBarUnderCover()
        MilkCrown.Kitsu.Anime.find(animeId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.displayAnimeDetail(it)
                    view.hideProgressBarUnderCover()
                }, {
                    Log.d("milk-crown",
                            "Error fetching anime detail ... .-. -> Anime id was $animeId " +
                                    "due to ${it.localizedMessage}")
                    view.hideProgressBarUnderCover()
                    val httpException = it as? HttpException ?: return@subscribe
                    Log.d("milk-crown", "httpException : ${httpException.response().body() as? String ?: "No body .-."}")
                })
    }

    override fun fetchAnimeReviews(animeId: String, view: AnimeDetailFeature.View) {
        view.showProgressBarUnderCover()
        MilkCrown.Kitsu.Anime.findReview(animeId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.displayAnimeReviewCount(it.meta["count"] as? Int ?: 0)
                    if (it.get().count() > 0) {
                        view.appendAnimeReviews(it.get())
                    }
                    view.hideProgressBarUnderCover()
                }, {
                    view.hideProgressBarUnderCover()
                })
    }

}