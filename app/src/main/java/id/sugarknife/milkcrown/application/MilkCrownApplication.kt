package id.sugarknife.milkcrown.application

import android.app.Application
import id.sugarknife.milkcrown.R
import uk.co.chrisjenx.calligraphy.CalligraphyConfig

class MilkCrownApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/delius-regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build())
    }

}