package id.sugarknife.milkcrown.util

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.Log
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import io.square1.richtextlib.spans.RemoteBitmapSpan

class RemoteBitmapSpanTarget(
        private val remoteBitmapSpan: RemoteBitmapSpan,
        private val context: Context) : SimpleTarget<Drawable>() {

    override fun onLoadStarted(placeholder: Drawable?) {
        super.onLoadStarted(placeholder)
        remoteBitmapSpan.updateBitmap(context, placeholder)
    }

    override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable>?) {
        Log.d("milk-crown", "image size (w * h) : ${resource.intrinsicWidth} * ${resource.intrinsicHeight}")
        remoteBitmapSpan.updateBitmap(context, resource)
    }

    override fun onLoadCleared(placeholder: Drawable?) {
        super.onLoadCleared(placeholder)
        remoteBitmapSpan.updateBitmap(context, placeholder)
    }

    override fun onLoadFailed(errorDrawable: Drawable?) {
        super.onLoadFailed(errorDrawable)
        remoteBitmapSpan.updateBitmap(context, errorDrawable)
    }

}