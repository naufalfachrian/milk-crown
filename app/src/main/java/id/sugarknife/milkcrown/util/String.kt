package id.sugarknife.milkcrown.util

fun String.formatBold(color: String) : String {
    return "<b><font color=\"$color\">$this</font></b>"
}