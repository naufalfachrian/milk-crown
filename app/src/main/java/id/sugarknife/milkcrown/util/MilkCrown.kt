package id.sugarknife.milkcrown.util

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.github.jasminb.jsonapi.retrofit.JSONAPIConverterFactory
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import id.sugarknife.milkcrown.model.kitsu.*
import id.sugarknife.milkcrown.network.AuthService
import id.sugarknife.milkcrown.network.KitsuService
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory

object MilkCrown {

    private val objectMapper by lazy {
        ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    }

    private val authRetrofit by lazy {
        Retrofit.Builder()
                .baseUrl("https://kitsu.io/api/")
                .addConverterFactory(JacksonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    private val retrofit by lazy {
        Retrofit.Builder()
                .baseUrl("https://kitsu.io/api/edge/")
                .addConverterFactory(JSONAPIConverterFactory(objectMapper,
                        ActivityGroupModel::class.java,
                        ActivityModel::class.java,
                        PostModel::class.java,
                        UserModel::class.java,
                        GroupModel::class.java,
                        CommentModel::class.java,
                        LibraryEntryModel::class.java,
                        MangaModel::class.java,
                        AnimeModel::class.java,
                        MediaReactionModel::class.java,
                        CategoryModel::class.java,
                        ReviewModel::class.java))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    object Kitsu {

        object Auth

        object User

        object Timeline

        object Post

        object Comment

        object MediaReaction

        object LibraryEntry

        object Anime

    }

    val SharedPreferencesKey = "sugarknife.milkcrown"

    val AuthRestApi by lazy { authRetrofit.create(AuthService::class.java) }

    val RestApi by lazy { retrofit.create(KitsuService::class.java) }

}