package id.sugarknife.milkcrown.util

import android.text.format.DateUtils
import java.util.*

fun printTimestamp(created: Date?, modified: Date?): CharSequence {
    val today = Date().time
    val displayDate: Long
    if (modified != null) displayDate = modified.time
    else if (created != null) displayDate = created.time
    else displayDate = today
    return DateUtils.getRelativeTimeSpanString(displayDate, today, DateUtils.MINUTE_IN_MILLIS)
}