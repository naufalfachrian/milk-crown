package id.sugarknife.milkcrown.util

import android.content.Context
import android.graphics.Point
import android.view.WindowManager

class DisplaySize {

    companion object {

        private var mScreenSize: Point? = null

        fun getScreenSize(context: Context): Point {
            val screenSize = mScreenSize
            if (screenSize != null) {
                return screenSize
            }
            val window = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val displaySize = Point()
            window.defaultDisplay.getSize(displaySize)
            mScreenSize = displaySize
            return displaySize
        }

    }

}