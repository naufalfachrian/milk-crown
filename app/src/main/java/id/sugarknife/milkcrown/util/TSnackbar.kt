package id.sugarknife.milkcrown.util

import android.graphics.Color
import android.util.TypedValue
import android.widget.LinearLayout
import android.widget.TextView
import com.androidadvance.topsnackbar.TSnackbar

fun TSnackbar.error(): TSnackbar {
    view.setBackgroundColor(Color.RED)
    val textView = view.findViewById<TextView>(com.androidadvance.topsnackbar.R.id.snackbar_text)
    textView.setTextColor(Color.WHITE)
    return this
}

fun TSnackbar.withPaddingTop(): TSnackbar {
    val textView = view.findViewById<TextView>(com.androidadvance.topsnackbar.R.id.snackbar_text)
    val wrapContent = LinearLayout.LayoutParams.WRAP_CONTENT
    val layoutParams = LinearLayout.LayoutParams(wrapContent, wrapContent)
    val res = view.context.resources
    val fourDpInPx = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4f, res.displayMetrics).toInt()
    val eightDpInPx = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8f, res.displayMetrics).toInt()
    val twentyEightDpInPx = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 28f, res.displayMetrics).toInt()
    layoutParams.setMargins(fourDpInPx, twentyEightDpInPx, fourDpInPx, eightDpInPx)
    textView.layoutParams = layoutParams
    return this
}