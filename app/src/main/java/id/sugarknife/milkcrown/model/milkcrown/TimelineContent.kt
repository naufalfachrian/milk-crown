package id.sugarknife.milkcrown.model.milkcrown

import id.sugarknife.milkcrown.model.kitsu.AnimeModel
import id.sugarknife.milkcrown.model.kitsu.MangaModel

interface TimelineContent {

    fun userAvatarUrl(): String?

    fun titleCaption(): String?

    fun timestampCaption(): String?

    fun embedVisibility(): Int

    fun content(): CharSequence?

    fun embedUrl(): String?

    fun embedDescription(): String?

    fun type(): Int

    fun mediaType(): String?

    fun mediaTitle(): String?

    fun mediaScore(): String?

    fun mediaScoreRank(): String?

    fun mediaPopularityRank(): String?

    fun mediaCover(): String?

    fun mediaId(): String?

    fun contentLikesCount(): Int

    fun contentReactionCount(): Int

    fun manga(): MangaModel?

    fun anime(): AnimeModel?

}
