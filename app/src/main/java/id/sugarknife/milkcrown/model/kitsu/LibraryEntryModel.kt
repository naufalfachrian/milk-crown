package id.sugarknife.milkcrown.model.kitsu

import android.view.View
import com.fasterxml.jackson.annotation.JsonIgnore
import com.github.jasminb.jsonapi.annotations.Id
import com.github.jasminb.jsonapi.annotations.Relationship
import com.github.jasminb.jsonapi.annotations.Type
import id.sugarknife.milkcrown.model.milkcrown.TimelineContent
import id.sugarknife.milkcrown.model.milkcrown.TimelineType
import id.sugarknife.milkcrown.util.printTimestamp
import java.util.*

@Type("libraryEntries")
class LibraryEntryModel : TimelineContent {

    @Id
    var id: String? = null

    var createdAt: Date? = null

    var updatedAt: Date? = null

    var status: String? = null

    var progress: Int = 0

    var volumesOwned: Int? = null

    var reconsuming: Boolean = false

    var reconsumeCount: Int = 0

    var notes: String? = null

    var private: Boolean = false

    var reactionSkipped: String? = null

    var progressedAt: String? = null

    var startedAt: String? = null

    var finishedAt: String? = null

    var ratingTwenty: Int? = null

    @JsonIgnore
    var foreignId: String? = null

    @Relationship("user")
    var user: UserModel? = null

    @Relationship("anime")
    var anime: AnimeModel? = null

    @Relationship("manga")
    var manga: MangaModel? = null

    private fun media(): LibraryMediaModel? {
        if (anime != null) return anime
        if (manga != null) return manga
        return null
    }

    override fun userAvatarUrl(): String? {
        return user?.avatar?.small
    }

    override fun titleCaption(): String? {
        var description = ""
        val status = this.foreignId ?: ""
        if (status.contains("progressed")) {
            description = if (anime != null) {
                "watched ${anime?.formattedTitle()} ep. ${progress}"
            } else {
                "read ${manga?.formattedTitle()} ch. ${progress}"
            }
        }
        if (status.contains("dropped")) {
            description = if (anime != null) {
                "dropped ${anime?.formattedTitle()} after ep. ${progress}"
            } else {
                "dropped ${manga?.formattedTitle()} after ch. ${progress}"
            }
        }
        if (status.contains("on_hold")) {
            description = if (anime != null) {
                "put ${anime?.formattedTitle()} on hold after ep. ${progress}"
            } else {
                "put ${manga?.formattedTitle()} on hold after ch. ${progress}"
            }
        }
        if (status.contains("completed")) {
            description = if (anime != null) {
                "finished ${anime?.formattedTitle()}"
            } else {
                "finished ${manga?.formattedTitle()}"
            }
        }
        if (status.contains("current")) {
            description = if (anime != null) {
                "started to watch ${anime?.formattedTitle()}"
            } else {
                "started to read ${manga?.formattedTitle()}"
            }
        }
        if (status.contains("planned")) {
            description = if (anime != null) {
                "planned to watch ${anime?.formattedTitle()}"
            } else {
                "planned to read ${manga?.formattedTitle()}"
            }
        }
        if (status.contains("rated")) {
            description = if (anime != null) {
                "rated ${anime?.formattedTitle()} ${ratingTwenty}"
            } else {
                "rated ${manga?.formattedTitle()} ${ratingTwenty}"
            }
        }
        return "${user?.formattedName()} $description"
    }

    override fun timestampCaption(): String? {
        return printTimestamp(createdAt, updatedAt).toString()
    }

    override fun embedVisibility(): Int {
        return View.GONE
    }

    override fun content(): CharSequence? {
        return null
    }

    override fun embedUrl(): String? {
        return null
    }

    override fun embedDescription(): String? {
        return null
    }

    override fun type(): Int {
        return TimelineType.LibraryEntry
    }

    override fun mediaCover(): String? {
        return media()?.posterImage?.small
    }

    override fun mediaPopularityRank(): String? {
        return media()?.popularityRank?.toString()
    }

    override fun mediaScore(): String? {
        return media()?.averageRating
    }

    override fun mediaScoreRank(): String? {
        return media()?.ratingRank?.toString()
    }

    override fun mediaTitle(): String? {
        return media()?.canonicalTitle
    }

    override fun mediaType(): String? {
        if (anime != null) return "Anime"
        if (manga != null) return "Manga"
        return null
    }

    override fun mediaId(): String? {
        return media()?.id
    }

    override fun contentLikesCount(): Int {
        return 0
    }

    override fun contentReactionCount(): Int {
        return 0
    }

    override fun anime(): AnimeModel? {
        return anime
    }

    override fun manga(): MangaModel? {
        return manga
    }

}
