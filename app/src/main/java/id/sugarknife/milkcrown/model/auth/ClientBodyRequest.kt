package id.sugarknife.milkcrown.model.auth

import com.fasterxml.jackson.annotation.JsonProperty

open class ClientBodyRequest {

    @JsonProperty("client_id")
    val clientId = "dd031b32d2f56c990b1425efe6c42ad847e7fe3ab46bf1299f05ecd856bdb7dd"

    @JsonProperty("client_secret")
    val clientSecret = "54d7307928f63414defd96399fc31ba847961ceaecef3a5fd93144e960c0e151"

}