package id.sugarknife.milkcrown.model.kitsu

import com.github.jasminb.jsonapi.annotations.Type

@Type("anime")
class AnimeModel : LibraryMediaModel() {

    var episodeCount: Int? = null

    var episodeLength: Int? = null

    var youtubeVideoId: String? = null

    var nsfw: Boolean? = null

}
