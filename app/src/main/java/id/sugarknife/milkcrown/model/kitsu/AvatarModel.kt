package id.sugarknife.milkcrown.model.kitsu

class AvatarModel {

    var tiny: String? = null

    var small: String? = null

    var medium: String? = null

    var large: String? = null

    var original: String? = null

}