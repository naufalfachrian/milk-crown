package id.sugarknife.milkcrown.model.kitsu

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonProperty
import com.github.jasminb.jsonapi.annotations.Id
import com.github.jasminb.jsonapi.annotations.Relationship
import com.github.jasminb.jsonapi.annotations.Type
import java.util.*

@Type("activityGroups")
class ActivityGroupModel {

    @Id
    var id: String? = null

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS")
    var createdAt: Date? = null

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS")
    var updatedAt: Date? = null

    var group: String? = null

    @JsonProperty("isSeen")
    var isSeen: String? = null

    @JsonProperty("isRead")
    var isRead: String? = null

    @Relationship("activities")
    var activities: List<ActivityModel>? = null

}