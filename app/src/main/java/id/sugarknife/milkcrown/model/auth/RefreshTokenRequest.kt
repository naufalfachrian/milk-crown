package id.sugarknife.milkcrown.model.auth

import com.fasterxml.jackson.annotation.JsonProperty

class RefreshTokenRequest(val refresh_token: String) : ClientBodyRequest() {

    @JsonProperty("grant_type")
    val grantType = "refresh_token"

}