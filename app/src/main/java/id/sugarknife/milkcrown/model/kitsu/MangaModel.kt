package id.sugarknife.milkcrown.model.kitsu

import com.github.jasminb.jsonapi.annotations.Type
import id.sugarknife.milkcrown.model.kitsu.LibraryMediaModel

@Type("manga")
class MangaModel : LibraryMediaModel() {

    var chapterCount: Int? = null

    var volumeCount: Int? = null

    var serialization: String? = null
}