package id.sugarknife.milkcrown.model.milkcrown

object TimelineType {

    const val LibraryEntry = 1

    const val MediaReaction = 2

    const val Post = 3

}