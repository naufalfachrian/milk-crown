package id.sugarknife.milkcrown.model.kitsu

import com.fasterxml.jackson.annotation.JsonFormat
import com.github.jasminb.jsonapi.annotations.Id
import com.github.jasminb.jsonapi.annotations.Type
import java.util.*

@Type("activities")
class ActivityModel {

    @Id
    var id: String? = null

    var status: String? = null

    var verb: String? = null

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    var time: Date? = null

    var streamId: String? = null

    var foreignId: String? = null

    var rating: String? = null

    var progress: String? = null

    var replyToType: String? = null

    var replyToUser: String? = null

    var nineteenScale: String? = null

    var mentionedUsers: List<String>? = null

}