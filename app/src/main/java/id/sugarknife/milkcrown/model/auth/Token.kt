package id.sugarknife.milkcrown.model.auth

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import io.reactivex.Observable
import java.util.*

@JsonIgnoreProperties(ignoreUnknown = true)
class Token (

        @JsonProperty("access_token")
        val accessToken: String?,

        @JsonProperty("expires_in")
        val lifetime: Number?,

        @JsonProperty("refresh_token")
        val refreshToken: String?,

        @JsonProperty("created_at")
        val timestamp: Number?

) {

    fun isValid(): Observable<Boolean> {
        if (lifetime == null) return Observable.just(false)
        if (timestamp == null) return Observable.just(false)
        val currentTime = Date().time / 1000
        val currentTokenLifetime = currentTime - timestamp.toLong()
        return if (currentTokenLifetime < lifetime.toLong() / 2) {
            Observable.just(true)
        } else {
            Observable.just(false)
        }
    }

}