package id.sugarknife.milkcrown.model.auth

import com.fasterxml.jackson.annotation.JsonProperty

class AuthRequest(val username: String, val password: String) : ClientBodyRequest() {

    @JsonProperty("grant_type")
    val grantType = "password"

}