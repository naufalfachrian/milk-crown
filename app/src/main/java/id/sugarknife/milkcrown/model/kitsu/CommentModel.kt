package id.sugarknife.milkcrown.model.kitsu

import android.view.View
import com.fasterxml.jackson.annotation.JsonFormat
import com.github.jasminb.jsonapi.annotations.Id
import com.github.jasminb.jsonapi.annotations.Relationship
import com.github.jasminb.jsonapi.annotations.Type
import id.sugarknife.milkcrown.model.milkcrown.TimelineContent
import id.sugarknife.milkcrown.model.milkcrown.TimelineType
import java.util.*

@Type("comments")
class CommentModel : TimelineContent {

    @Id
    var id: String? = null

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    var createdAt: Date? = null

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    var updatedAt: Date? = null

    var content: String? = null

    var contentFormatted: String? = null

    var blocked: Boolean = false

    var deletedAt: String? = null

    var likesCount = 0

    var repliesCount = 0

    var editedAt: String? = null

    var embed: EmbedModel? = null

    var embedUrl: String? = null

    @Relationship("user")
    var user: UserModel? = null

    @Relationship("post")
    var post: PostModel? = null

    @Relationship("parent")
    var parent: CommentModel? = null

    @Relationship("likes")
    var likes: List<UserModel>? = null

    @Relationship("replies")
    var replies: List<CommentModel>? = null

    override fun userAvatarUrl(): String? {
        return post?.userAvatarUrl()
    }

    override fun titleCaption(): String? {
        return post?.titleCaption()
    }

    override fun timestampCaption(): String? {
        return post?.timestampCaption()
    }

    override fun embedVisibility(): Int {
        return post?.embedVisibility() ?: View.GONE
    }

    override fun content(): CharSequence? {
        return post?.content()
    }

    override fun embedUrl(): String? {
        return post?.embedUrl()
    }

    override fun embedDescription(): String? {
        return post?.embedDescription()
    }

    override fun type(): Int {
        return TimelineType.Post
    }

    override fun mediaCover(): String? {
        return post?.mediaCover()
    }

    override fun mediaPopularityRank(): String? {
        return post?.mediaPopularityRank()
    }

    override fun mediaScore(): String? {
        return post?.mediaScore()
    }

    override fun mediaScoreRank(): String? {
        return post?.mediaScoreRank()
    }

    override fun mediaTitle(): String? {
        return post?.mediaTitle()
    }

    override fun mediaType(): String? {
        return post?.mediaType()
    }

    override fun mediaId(): String? {
        return post?.mediaId()
    }

    override fun contentLikesCount(): Int {
        return post?.contentLikesCount() ?: 0
    }

    override fun contentReactionCount(): Int {
        return post?.contentReactionCount() ?: 0
    }

    override fun anime(): AnimeModel? {
        return post?.anime()
    }

    override fun manga(): MangaModel? {
        return post?.manga()
    }

}
