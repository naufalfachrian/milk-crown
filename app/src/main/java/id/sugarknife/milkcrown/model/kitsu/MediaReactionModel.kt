package id.sugarknife.milkcrown.model.kitsu

import android.view.View
import com.fasterxml.jackson.annotation.JsonFormat
import com.github.jasminb.jsonapi.annotations.Id
import com.github.jasminb.jsonapi.annotations.Relationship
import com.github.jasminb.jsonapi.annotations.Type
import id.sugarknife.milkcrown.model.milkcrown.TimelineContent
import id.sugarknife.milkcrown.model.milkcrown.TimelineType
import id.sugarknife.milkcrown.util.printTimestamp
import java.util.*

@Type("mediaReactions")
class MediaReactionModel : TimelineContent {

    @Id
    var id: String? = null

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    var createdAt: Date? = null

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    var updatedAt: Date? = null

    var reaction: String? = null

    var upVotesCount: Int? = null

    @Relationship("anime")
    var anime: AnimeModel? = null

    @Relationship("manga")
    var manga: MangaModel? = null

    @Relationship("user")
    var user: UserModel? = null

    private fun media(): LibraryMediaModel? {
        if (anime != null) return anime
        if (manga != null) return manga
        return null
    }

    override fun userAvatarUrl(): String? {
        return user?.avatar?.small
    }

    override fun titleCaption(): String? {
        val media = if (anime != null) { anime } else { manga }
        return "${user?.formattedName()} reacted to ${media?.formattedTitle()}"
    }

    override fun timestampCaption(): String? {
        return printTimestamp(createdAt, updatedAt).toString()
    }

    override fun embedVisibility(): Int {
        return View.GONE
    }

    override fun content(): CharSequence? {
        return reaction
    }

    override fun embedUrl(): String? {
        return null
    }

    override fun embedDescription(): String? {
        return null
    }

    override fun type(): Int {
        return TimelineType.MediaReaction
    }

    override fun mediaCover(): String? {
        return media()?.posterImage?.small
    }

    override fun mediaPopularityRank(): String? {
        return media()?.popularityRank.toString()
    }

    override fun mediaScore(): String? {
        return media()?.averageRating
    }

    override fun mediaScoreRank(): String? {
        return media()?.ratingRank.toString()
    }

    override fun mediaTitle(): String? {
        return media()?.canonicalTitle
    }

    override fun mediaType(): String? {
        if (anime != null) return "Anime"
        if (manga != null) return "Manga"
        return null
    }

    override fun mediaId(): String? {
        return media()?.id
    }

    override fun contentLikesCount(): Int {
        return upVotesCount?.toString()?.toIntOrNull() ?: 0
    }

    override fun contentReactionCount(): Int {
        return 0
    }

    override fun anime(): AnimeModel? {
        return anime
    }

    override fun manga(): MangaModel? {
        return manga
    }

}