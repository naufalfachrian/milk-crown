package id.sugarknife.milkcrown.model.kitsu

import com.fasterxml.jackson.annotation.JsonFormat
import com.github.jasminb.jsonapi.annotations.Id
import com.github.jasminb.jsonapi.annotations.Relationship
import com.github.jasminb.jsonapi.annotations.Type
import java.util.*

@Type("categories")
class CategoryModel {

    @Id
    var id: String? = null

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    var createdAt: Date? = null

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    var updatedAt: Date? = null

    var title: String? = null

    var description: String? = null

    var totalMediaCount: Int = 0

    var slug: String? = null

    var nsfw: Boolean = false

    @Relationship("parent")
    var parent: CategoryModel? = null

}