package id.sugarknife.milkcrown.model.milkcrown

class MediaViewModel(content: TimelineContent) {

    val type: String = content.mediaType() ?: ""

    val title: CharSequence = content.mediaTitle() ?: ""

    val score: CharSequence = content.mediaScore() ?: ""

    val scoreRank: CharSequence = content.mediaScoreRank() ?: ""

    val popularityRank: CharSequence = content.mediaPopularityRank() ?: ""

    val coverUrl: String = content.mediaCover() ?: ""

    val posterUrl: String = content.anime()?.coverImage?.original ?: content.manga()?.coverImage?.original ?: ""

    val id: String = content.mediaId() ?: ""

}