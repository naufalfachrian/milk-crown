package id.sugarknife.milkcrown.model.auth

import io.reactivex.Observable

class Session(val token: Token, val userId: String) {

    companion object {

        fun create(token: Token?, userId: String?): Observable<Session> {
            return if (token == null || userId == null) {
                Observable.error(RuntimeException("Session broken."))
            } else {
                Observable.just(Session(token, userId))
            }
        }

    }

}