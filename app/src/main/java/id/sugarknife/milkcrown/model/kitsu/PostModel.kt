package id.sugarknife.milkcrown.model.kitsu

import android.view.View
import com.fasterxml.jackson.annotation.JsonFormat
import com.github.jasminb.jsonapi.annotations.Id
import com.github.jasminb.jsonapi.annotations.Relationship
import com.github.jasminb.jsonapi.annotations.Type
import id.sugarknife.milkcrown.model.milkcrown.TimelineContent
import id.sugarknife.milkcrown.model.milkcrown.TimelineType
import id.sugarknife.milkcrown.util.formatBold
import id.sugarknife.milkcrown.util.printTimestamp
import java.util.*

@Type("posts")
class PostModel : TimelineContent {

    @Id
    var id: String? = null

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    var createdAt: Date? = null

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    var updatedAt: Date? = null

    var content: String? = null

    var contentFormatted: String? = null

    var commentsCount: Int = 0

    var postLikesCount: Int = 0

    var spoiler: Boolean = false

    var nsfw: Boolean = false

    var blocked: Boolean = false

    var deletedAt: String? = null

    var topLevelCommentsCount: Int = 0

    var editedAt: String? = null

    var targetInterest: String? = null

    var embed: EmbedModel? = null

    var embedUrl: String? = null

    @Relationship("user")
    var user: UserModel? = null

    @Relationship("targetUser")
    var targetUser: UserModel? = null

    @Relationship("targetGroup")
    var targetGroup: GroupModel? = null

    @Relationship("media")
    var anime: AnimeModel? = null

    @Relationship("media")
    var manga: MangaModel? = null

    private fun media(): LibraryMediaModel? {
        if (anime != null) {
            return anime
        }
        if (manga != null) {
            return manga
        }
        return null
    }

    override fun userAvatarUrl(): String? {
        return user?.avatar?.small
    }

    override fun titleCaption(): String? {
        val suffixTitle: String = if (targetUser == null && targetGroup == null) {
            if (user?.gender == "male") {
                "his profile".formatBold("#402f3f")
            } else {
                "her profile".formatBold("#402f3f")
            }
        } else {
            if (targetGroup != null) {
                "${targetGroup?.name}".formatBold("#402f3f")
            } else {
                "${targetUser?.name}".formatBold("#402f3f") + "\'s profile"
            }
        }
        val mediaTitle: String = if (anime == null && manga == null) {
            ""
        } else {
            if (anime != null) {
                "about ${anime?.formattedTitle()}"
            } else {
                "about ${manga?.formattedTitle()}"
            }
        }
        return "${user?.formattedName()} posted on $suffixTitle $mediaTitle"
    }

    override fun timestampCaption(): String? {
        return printTimestamp(createdAt, updatedAt).toString()
    }

    override fun embedVisibility(): Int {
        return if (embed == null) { View.GONE } else { View.VISIBLE }
    }

    override fun content(): CharSequence? {
        return contentFormatted
    }

    override fun embedUrl(): String? {
        return embed?.image?.url
    }

    override fun embedDescription(): String? {
        if (embed?.siteName == null) {
            return embed?.title
        }
        return "${embed?.title} - ${embed?.siteName}"
    }

    override fun type(): Int {
        return TimelineType.Post
    }

    override fun mediaCover(): String? {
        return media()?.posterImage?.small
    }

    override fun mediaPopularityRank(): String? {
        return media()?.popularityRank.toString()
    }

    override fun mediaScore(): String? {
        return media()?.averageRating
    }

    override fun mediaScoreRank(): String? {
        return media()?.ratingRank?.toString()
    }

    override fun mediaTitle(): String? {
        return media()?.formattedTitle()
    }

    override fun mediaType(): String? {
        if (anime != null) return "Anime"
        if (manga != null) return "Manga"
        return null
    }

    override fun mediaId(): String? {
        return media()?.id
    }

    override fun contentLikesCount(): Int {
        return postLikesCount
    }

    override fun contentReactionCount(): Int {
        return topLevelCommentsCount
    }

    override fun anime(): AnimeModel? {
        return anime
    }

    override fun manga(): MangaModel? {
        return manga
    }

}