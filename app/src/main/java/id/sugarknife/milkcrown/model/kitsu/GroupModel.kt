package id.sugarknife.milkcrown.model.kitsu

import com.fasterxml.jackson.annotation.JsonFormat
import com.github.jasminb.jsonapi.annotations.Id
import com.github.jasminb.jsonapi.annotations.Type
import id.sugarknife.milkcrown.model.kitsu.AvatarModel
import java.util.*

@Type("groups")
class GroupModel {

    @Id
    var id: String? = null

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    var createdAt: Date? = null

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    var updatedAt: Date? = null

    var slug: String? = null

    var about: String? = null

    var locale: String? = null

    var membersCount = 0

    var name: String? = null

    var nsfw = false

    var privacy: String? = null

    var rules: String? = null

    var rulesFormatted: String? = null

    var leadersCount = 0

    var neighborsCount = 0

    var featured = false

    var tagline: String? = null

    var lastActivityAt: String? = null

    var avatar: AvatarModel? = null

    var coverImage: AvatarModel? = null

}