package id.sugarknife.milkcrown.model.milkcrown

data class TimelineResponse(val timelineList: List<TimelineViewModel>, val lastCursor: String)