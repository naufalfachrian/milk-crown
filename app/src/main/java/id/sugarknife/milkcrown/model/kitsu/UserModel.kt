package id.sugarknife.milkcrown.model.kitsu

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.github.jasminb.jsonapi.annotations.Id
import com.github.jasminb.jsonapi.annotations.Type
import id.sugarknife.milkcrown.util.formatBold
import java.util.*

@Type("users")
@JsonIgnoreProperties("lifeSpentOnAnime", "website")
class UserModel {

    @Id
    var id: String? = null

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    var createdAt: Date? = null

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    var updatedAt: Date? = null

    var name: String? = null

    var pastNames: List<String>? = null

    var slug: String? = null

    var about: String? = null

    var location: String? = null

    var waifuOrHusbando: String? = null

    var followersCount: Int = 0

    var followingCount: Int = 0

    var birthday: String? = null

    var gender: String? = null

    var commentsCount: Int = 0

    var favoritesCount: Int = 0

    var likesGivenCount: Int = 0

    var reviewsCount: Int = 0

    var likesReceivedCount: Int = 0

    var postsCount: Int = 0

    var ratingsCount: Int = 0

    var mediaReactionsCount: Int = 0

    var proExpiresAt: String? = null

    var title: String? = null

    var profileCompleted: String? = null

    var feedCompleted: String? = null

    var avatar: AvatarModel? = null

    var coverImage: AvatarModel? = null

    var ratingSystem: String? = null

    var facebookId: String? = null

    fun formattedName() : String {
        if (gender == "male") {
            return name?.formatBold("#2196F3") ?: ""
        }
        return name?.formatBold("#E91E63") ?: ""
    }

}