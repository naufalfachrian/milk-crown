package id.sugarknife.milkcrown.model.kitsu

import com.fasterxml.jackson.annotation.JsonFormat
import com.github.jasminb.jsonapi.annotations.Id
import com.github.jasminb.jsonapi.annotations.Relationship
import com.github.jasminb.jsonapi.annotations.Type
import java.util.*

@Type("reviews")
class ReviewModel {

    @Id
    var id: String? = null

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS")
    var createdAt: Date? = null

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS")
    var updatedAt: Date? = null

    var content: String? = null

    var contentFormatted: String? = null

    var likesCount: Int = 0

    var rating: Double = 0.0

    var spoiler: Boolean = false

    @Relationship("user")
    var createdBy: UserModel? = null

}
