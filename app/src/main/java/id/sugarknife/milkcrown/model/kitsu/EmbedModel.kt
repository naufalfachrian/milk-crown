package id.sugarknife.milkcrown.model.kitsu

import com.fasterxml.jackson.annotation.JsonProperty

class EmbedModel {

    var kind: String? = null

    var image: ImageEmbedModel? = null

    var title: String? = null

    var video: VideoEmbedModel? = null

    @JsonProperty("site_name")
    var siteName: String? = null

    var description: String? = null

    inner class ImageEmbedModel {

        var url: String? = null

    }

    inner class VideoEmbedModel {

        var url: String? = null

        var type: String? = null

        var width: Int = 0

        var height: Int = 0

    }

}