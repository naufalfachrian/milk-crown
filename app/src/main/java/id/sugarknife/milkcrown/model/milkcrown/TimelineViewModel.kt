package id.sugarknife.milkcrown.model.milkcrown

import android.os.Build
import android.text.Html

class TimelineViewModel(content: TimelineContent) {

    val userAvatarUrl: String = content.userAvatarUrl() ?: ""

    val title: CharSequence = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(content.titleCaption(), Html.FROM_HTML_MODE_LEGACY)
    } else {
        @Suppress("DEPRECATION")
        Html.fromHtml(content.titleCaption())
    } ?: ""

    val timestamp: CharSequence = content.timestampCaption() ?: ""

    val content: CharSequence = content.content() ?: ""

    val type: Int = content.type()

    val media = MediaViewModel(content)

    val likesCount = content.contentLikesCount()

    val reactionCount = content.contentReactionCount()

}