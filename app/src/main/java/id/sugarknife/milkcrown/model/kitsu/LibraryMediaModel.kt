package id.sugarknife.milkcrown.model.kitsu

import com.fasterxml.jackson.annotation.JsonFormat
import com.github.jasminb.jsonapi.annotations.Id
import com.github.jasminb.jsonapi.annotations.Relationship
import id.sugarknife.milkcrown.util.formatBold
import java.text.SimpleDateFormat
import java.util.*

open class LibraryMediaModel {

    @Id
    var id: String? = null

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    var createdAt: Date? = null

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    var updatedAt: Date? = null

    var slug: String? = null

    var synopsis: String? = null

    var titles: MediaTitleModel? = null

    var canonicalTitle: String? = null

    var abbreviatedTitles: List<String>? = null

    var averageRating: String? = null

    var userCount: Int? = null

    var favoritesCount: Int? = null

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    var startDate: Date? = null

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    var endDate: Date? = null

    var popularityRank: Int? = null

    var ratingRank: Int? = null

    var ageRating: String? = null

    var ageRatingGuide: String? = null

    var subtype: String? = null

    var status: String? = null

    var tba: String? = null

    var posterImage: AvatarModel? = null

    var coverImage: AvatarModel? = null

    @Relationship("categories")
    var categories: List<CategoryModel>? = null

    fun formattedTitle() : String {
        return canonicalTitle?.formatBold("#FF5722") ?: ""
    }

    fun formattedStartDate() : String {
        if (startDate != null) {
            val simpleDateFormatter = SimpleDateFormat("MMM d, yyyy", Locale.US)
            return simpleDateFormatter.format(startDate)
        }
        return ""
    }

    fun formattedEndDate() : String {
        if (endDate != null) {
            val simpleDateFormatter = SimpleDateFormat("MMM d, yyyy", Locale.US)
            return simpleDateFormatter.format(endDate)
        }
        return ""
    }

}