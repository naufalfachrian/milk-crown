package id.sugarknife.milkcrown.model.kitsu

import com.fasterxml.jackson.annotation.JsonProperty

class MediaTitleModel {

    @JsonProperty("en")
    var englishTitle: String? = null

    @JsonProperty("en_jp")
    var romajiTitle: String? = null

    @JsonProperty("ja_jp")
    var japaneseTitle: String? = null

}