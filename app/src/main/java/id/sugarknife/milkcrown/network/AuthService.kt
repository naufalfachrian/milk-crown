package id.sugarknife.milkcrown.network

import id.sugarknife.milkcrown.model.auth.AuthRequest
import id.sugarknife.milkcrown.model.auth.RefreshTokenRequest
import id.sugarknife.milkcrown.model.auth.Token
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthService {

    @POST("oauth/token")
    fun requestToken(@Body authRequest: AuthRequest): Observable<Token>

    @POST("oauth/token")
    fun refreshToken(@Body refreshTokenRequest: RefreshTokenRequest): Observable<Token>

}