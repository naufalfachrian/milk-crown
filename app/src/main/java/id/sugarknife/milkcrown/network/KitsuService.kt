package id.sugarknife.milkcrown.network

import com.github.jasminb.jsonapi.JSONAPIDocument
import id.sugarknife.milkcrown.model.kitsu.*
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

interface KitsuService {

    @GET("feeds/timeline/{userId}")
    fun fetchTimeline(

            @Header("Authorization")
            bearerToken: String,

            @Path("userId")
            userId: String,

            @Query("page[cursor]")
            lastCursor: String = "",

            @Query("page[limit]")
            itemsPerPage: Int = 10,

            @Query("include")
            includeRelationship: String = "activities")

            : Observable<JSONAPIDocument<List<ActivityGroupModel>>>


    @GET("users")
    fun searchUser(@Query("filter[name]") query: String): Observable<JSONAPIDocument<List<UserModel>>>


    @GET("library-entries/{libraryEntryId}")
    fun findLibraryEntry(

            @Path("libraryEntryId")
            libraryEntryId: String,

            @Query("include")
            includeRelationship: String = "user,anime,manga")

            : Observable<JSONAPIDocument<LibraryEntryModel>>


    @GET("posts/{postId}")
    fun findPost(

            @Path("postId")
            postId: String,

            @Query("include")
            includeRelationship: String = "user,targetUser,targetGroup,media")

            : Observable<JSONAPIDocument<PostModel>>


    @GET("comments/{commentId}")
    fun findComment(

            @Path("commentId")
            commentId: String,

            @Query("include")
            includeRelationship: String = "user,post,parent,replies,post.user,post.targetUser,post.targetGroup,post.media")

            : Observable<JSONAPIDocument<CommentModel>>


    @GET("media-reactions/{mediaReactionId}")
    fun findMediaReaction(

            @Path("mediaReactionId")
            libraryEntryId: String,

            @Query("include")
            includeRelationship: String = "user,anime,manga")

            : Observable<JSONAPIDocument<MediaReactionModel>>

    @GET("anime/{animeId}")
    fun findAnime(

            @Path("animeId")
            animeId: String,

            @Query("include")
            includeRelationship: String = "categories",

            @Query("fields[categories]")
            includeCategoryFields: String = "title")

            : Observable<JSONAPIDocument<AnimeModel>>

    @GET("anime/{animeId}/reviews")
    fun findReviewForAnime(

            @Path("animeId")
            animeId: String,

            @Query("page[offset]")
            offset: Int = 0,

            @Query("page[limit]")
            limit: Int = 10,

            @Query("include")
            includeRelationship: String = "user",

            @Query("fields[users]")
            includeCategoryFields: String = "name,avatar,gender")

            : Observable<JSONAPIDocument<List<ReviewModel>>>

}