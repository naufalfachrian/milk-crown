package id.sugarknife.milkcrown.controller

import android.content.Context
import id.sugarknife.milkcrown.model.auth.AuthRequest
import id.sugarknife.milkcrown.model.auth.RefreshTokenRequest
import id.sugarknife.milkcrown.model.auth.Session
import id.sugarknife.milkcrown.model.auth.Token
import id.sugarknife.milkcrown.util.MilkCrown
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

private object PreferencesKey {

    val accessTokenKey = "token.access-token"

    val refreshTokenKey = "token.refresh-token"

    val lifetimeTokenKey = "token.lifetime"

    val timestampTokenKey = "token.created-time"

    val userIdKey = "session.user-id"

}

fun MilkCrown.Kitsu.Auth.login(username: String, password: String): Observable<Session> {
    var token: Token? = null
    return MilkCrown.AuthRestApi.requestToken(AuthRequest(username, password))
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .flatMap {
                token = it
                MilkCrown.Kitsu.User.findByUsername(username)
            }
            .flatMap {
                Session.create(token, it.id)
            }
}


fun MilkCrown.Kitsu.Auth.checkAndRefreshSessionIfNeeded(session: Session): Observable<Session> {
    return session.token.isValid()
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .flatMap {
                if (it) {
                    Observable.just(session)
                } else {
                    MilkCrown.AuthRestApi.refreshToken(RefreshTokenRequest(session.token.refreshToken ?: ""))
                            .subscribeOn(Schedulers.io())
                            .observeOn(Schedulers.io())
                            .flatMap {
                                Observable.just(Session(it, session.userId))
                            }
                }
            }
}

fun MilkCrown.Kitsu.Auth.loadSession(context: Context): Observable<Session> {
    val preferences = context.getSharedPreferences(MilkCrown.SharedPreferencesKey, Context.MODE_PRIVATE)
    val accessToken = preferences.getString(PreferencesKey.accessTokenKey, "")
    val refreshToken = preferences.getString(PreferencesKey.refreshTokenKey, "")
    val lifetimeToken = preferences.getInt(PreferencesKey.lifetimeTokenKey, 0)
    val timestampToken = preferences.getInt(PreferencesKey.timestampTokenKey, 0)
    val userId = preferences.getString(PreferencesKey.userIdKey, "")
    return if (accessToken.isEmpty() || refreshToken.isEmpty()
            || lifetimeToken == 0 || timestampToken == 0 || userId.isEmpty()) {
        Observable.error(RuntimeException("No session available."))
    } else {
        Observable.just(Session(Token(accessToken, lifetimeToken, refreshToken, timestampToken), userId))
    }
}

fun MilkCrown.Kitsu.Auth.saveSession(context: Context, session: Session): Observable<Session> {
    val sharedPreferences = context.getSharedPreferences(MilkCrown.SharedPreferencesKey, Context.MODE_PRIVATE)
    val saved = sharedPreferences.edit()
            .putString(PreferencesKey.accessTokenKey, session.token.accessToken ?: "")
            .putString(PreferencesKey.refreshTokenKey, session.token.refreshToken ?: "")
            .putInt(PreferencesKey.lifetimeTokenKey, session.token.lifetime?.toInt() ?: 0)
            .putInt(PreferencesKey.timestampTokenKey, session.token.timestamp?.toInt() ?: 0)
            .putString(PreferencesKey.userIdKey, session.userId)
            .commit()
    return if (saved) {
        Observable.just(session)
    } else {
        Observable.error(RuntimeException("Failed to save session."))
    }
}
