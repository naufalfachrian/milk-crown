package id.sugarknife.milkcrown.controller

import com.github.jasminb.jsonapi.JSONAPIDocument
import id.sugarknife.milkcrown.model.kitsu.AnimeModel
import id.sugarknife.milkcrown.model.kitsu.ReviewModel
import id.sugarknife.milkcrown.util.MilkCrown
import io.reactivex.Observable

fun MilkCrown.Kitsu.Anime.find(id: String): Observable<AnimeModel> {
    return MilkCrown.RestApi.findAnime(id)
            .flatMap {
                Observable.just(it.get())
            }
}

fun MilkCrown.Kitsu.Anime.findReview(animeId: String, offset: Int = 0): Observable<JSONAPIDocument<List<ReviewModel>>> {
    return MilkCrown.RestApi.findReviewForAnime(animeId, offset)
}