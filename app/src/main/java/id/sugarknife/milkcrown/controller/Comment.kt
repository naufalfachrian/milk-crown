package id.sugarknife.milkcrown.controller

import id.sugarknife.milkcrown.model.kitsu.CommentModel
import id.sugarknife.milkcrown.util.MilkCrown
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

fun MilkCrown.Kitsu.Comment.find(id: String): Observable<CommentModel> {
    return MilkCrown.RestApi.findComment(id)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .flatMap {
                Observable.just(it.get())
            }
}