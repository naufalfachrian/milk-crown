package id.sugarknife.milkcrown.controller

import id.sugarknife.milkcrown.model.kitsu.MediaReactionModel
import id.sugarknife.milkcrown.util.MilkCrown
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

fun MilkCrown.Kitsu.MediaReaction.find(id: String): Observable<MediaReactionModel> {
    return MilkCrown.RestApi.findMediaReaction(id)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .flatMap {
                Observable.just(it.get())
            }
}