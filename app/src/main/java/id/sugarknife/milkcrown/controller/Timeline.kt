package id.sugarknife.milkcrown.controller

import android.content.Context
import android.util.Log
import id.sugarknife.milkcrown.model.kitsu.ActivityGroupModel
import id.sugarknife.milkcrown.model.milkcrown.TimelineResponse
import id.sugarknife.milkcrown.model.milkcrown.TimelineViewModel
import id.sugarknife.milkcrown.util.MilkCrown
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

fun MilkCrown.Kitsu.Timeline.fetchTimeline(context: Context, lastCursor: String): Observable<TimelineResponse> {
    return MilkCrown.Kitsu.Auth.loadSession(context)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .flatMap {
                MilkCrown.RestApi.fetchTimeline("Bearer ${it.token.accessToken}", it.userId, lastCursor)
                        .flatMap {
                            parseTimeline(it.get())
                        }
            }
}

private fun MilkCrown.Kitsu.Timeline.parseTimeline(items: List<ActivityGroupModel>): Observable<TimelineResponse> {
    val timelineItems = ArrayList<TimelineViewModel>()
    var lastCursor: String? = null
    for (item in items) {
        try {
            val activity = item.activities?.first() ?: break
            val split = activity.foreignId?.split(":")
            val foreignModel = split?.get(0)
            val foreignId = split?.get(1) ?: ""
            when (foreignModel) {
                "Comment" -> {
                    MilkCrown.Kitsu.Comment.find(foreignId)
                            .blockingSubscribe({
                                timelineItems.add(TimelineViewModel(it))
                            }, {})
                }
                "LibraryEntry" -> {
                    MilkCrown.Kitsu.LibraryEntry.find(foreignId)
                            .blockingSubscribe({
                                it.foreignId = activity.foreignId
                                timelineItems.add(TimelineViewModel(it))
                            }, {})
                }
                "MediaReaction" -> {
                    MilkCrown.Kitsu.MediaReaction.find(foreignId)
                            .blockingSubscribe({
                                timelineItems.add(TimelineViewModel(it))
                            }, {})
                }
                "Post" -> {
                    MilkCrown.Kitsu.Post.find(foreignId)
                            .blockingSubscribe({
                                timelineItems.add(TimelineViewModel(it))
                            }, {})
                }
            }
            lastCursor = item.id
        } catch (exception: Exception) {
            Log.e("milk-crown", exception.localizedMessage)
        }
    }
    return Observable.just(TimelineResponse(timelineItems, lastCursor ?: ""))
}
