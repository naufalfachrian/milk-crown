package id.sugarknife.milkcrown.controller

import id.sugarknife.milkcrown.model.kitsu.LibraryEntryModel
import id.sugarknife.milkcrown.util.MilkCrown
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

fun MilkCrown.Kitsu.LibraryEntry.find(id: String): Observable<LibraryEntryModel> {
    return MilkCrown.RestApi.findLibraryEntry(id)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .flatMap {
                Observable.just(it.get())
            }
}