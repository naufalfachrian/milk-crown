package id.sugarknife.milkcrown.controller

import id.sugarknife.milkcrown.model.kitsu.UserModel
import id.sugarknife.milkcrown.util.MilkCrown
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

fun MilkCrown.Kitsu.User.findByUsername(username: String): Observable<UserModel> {
    return MilkCrown.RestApi.searchUser(username)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .flatMap {
                Observable.just(it.get().first())
            }
            .flatMap {
                if (it.name == username) {
                    Observable.just(it)
                } else {
                    Observable.error(RuntimeException("Couldn't find $username"))
                }
            }
}