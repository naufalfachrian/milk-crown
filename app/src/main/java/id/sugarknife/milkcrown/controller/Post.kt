package id.sugarknife.milkcrown.controller

import id.sugarknife.milkcrown.model.kitsu.PostModel
import id.sugarknife.milkcrown.util.MilkCrown
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

fun MilkCrown.Kitsu.Post.find(id: String): Observable<PostModel> {
    return MilkCrown.RestApi.findPost(id)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .flatMap {
                Observable.just(it.get())
            }
}